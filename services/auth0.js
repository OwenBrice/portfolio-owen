// src/Auth/Auth.js

import auth0 from 'auth0-js';
import Cookies from 'js-cookie';
import jwt from 'jsonwebtoken';

class Auth {
  constructor() {
    this.auth0 = new auth0.WebAuth({
      domain: 'dev-l8hagjsa.auth0.com',
      clientID: '26CwcPmRQ36NRhgbt1FWTl4qILksN05l',
      redirectUri: 'http://localhost:3000/callback',
      responseType: 'token id_token',
      scope: 'openid profile'
    });
  }

  handleAuthentication = () => {
    //debugger;
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
          this.setSession(authResult);
          resolve();
        } else if (err) {
          reject(err);
          console.log(err);
          alert(`Error: ${err.error}. Check the console for further details.`);
        }
      });
    });
  };

  setSession = authResult => {
    // Set isLoggedIn flag in localStorage
    //localStorage.setItem('isLoggedIn', 'true');

    // Set the time that the access token will expire at
    const expiresAt = authResult.expiresIn * 1000 + new Date().getTime();
    //this.accessToken = authResult.accessToken;
    // this.idToken = authResult.idToken;
    // this.expiresAt = expiresAt;

    Cookies.set('user', authResult.idTokenPayload);
    Cookies.set('jwt', authResult.idToken);
    Cookies.set('expiresAt', expiresAt);
  };

  login = () => {
    this.auth0.authorize();
  };

  logout = () => {
    Cookies.remove('user');
    Cookies.remove('jwt');
    Cookies.remove('expiresAt');
    // Remove tokens and expiry time
    // this.accessToken = null;
    // this.idToken = null;
    // this.expiresAt = 0;

    // Remove isLoggedIn flag from localStorage
    //localStorage.removeItem('isLoggedIn');

    this.auth0.logout({
      return_to: '',
      clientID: '26CwcPmRQ36NRhgbt1FWTl4qILksN05l'
    });
  };

  isAuthenticated = () => {
    // Check whether the current time is past the
    // access token's expiry time
    //let expiresAt = this.expiresAt;
    const expiresAt = Cookies.getJSON('expiresAt');
    return new Date().getTime() < expiresAt;
  };

  // Use verifyToken instead isAuthenticated for better security
  verifyToken = token => {
    if (token) {
      const decodedToken = jwt.decode(token);
      const expiresAt = decodedToken.exp * 1000;

      return decodedToken && new Date().getTime() < expiresAt
        ? decodedToken
        : undefined;
    }

    return undefined;
  };

  clientAuth = () => {
    //this.isAuthenticated();
    const token = Cookies.getJSON('jwt');
    const verifiedToken = this.verifyToken(token);

    return verifiedToken;
  };

  serverAuth = req => {
    if (req.headers.cookie) {
      const tokenCookie = req.headers.cookie
        .split(';')
        .find(c => c.trim().startsWith('jwt='));

      if (!tokenCookie) {
        return undefined;
      }

      const token = tokenCookie.split('=')[1];
      const verifiedToken = this.verifyToken(token);

      return verifiedToken;
    }

    return undefined;

    // if (req.headers.cookie) {
    //   const expiresAtCookie = req.headers.cookie
    //     .split(';')
    //     .find(c => c.trim().startsWith('expiresAt='));

    //   if (!expiresAtCookie) {
    //     return undefined;
    //   }

    //   const expiresAt = expiresAtCookie.split('=')[1];
    //   return new Date().getTime() < expiresAt;
    // }
  };
}

const auth0Client = new Auth();

export default auth0Client;
